# mkdocs-gitlab-static-site-editor

A tiny MkDocs plugin that enables the
[GitLab Static Site Editor](https://docs.gitlab.com/ee/user/project/static_site_editor/)
to edit your MkDocs pages. This is useful for contributions from non-technical users.

This is a drop-in replacement that should work with any theme, by changing the links in
the Edit button to use the Static Site Editor rather than the default the single-page editor.

As with the default MkDocs behavior, the Edit button will not appear unless the `repo_url`
option is defined by the user in `mkdocs.yml`.

## Quickstart

Install the plugin:

`pip install git+https://gitlab.com/nejch1/mkdocs-gitlab-static-site-editor`

Configure your `repo_url` and enable the plugin in `mkdocs.yml`:

```yaml
repo_url: https://gitlab.com/username/your-mkdocs-project

plugins:
  - search
  - gitlab-static-site-editor
```

> **Note:** If you have no `plugins` entry in your config file yet, you'll likely also want to add the `search` plugin.
> MkDocs enables it by default if there is no `plugins` entry set, but now you have to enable it explicitly.

Alternatively, you can customize the branch to edit in the Static Site Editor:

```yaml
repo_url: https://gitlab.com/username/your-mkdocs-project

plugins:
  - search
  - gitlab-static-site-editor:
      edit_branch: main
```

Configure the image upload path to be inside `docs/` using `.gitlab/static-site-editor.yml`<sup>1</sup> (see 
[details](https://docs.gitlab.com/ee/user/project/static_site_editor/#static-site-editor-configuration-file)):

```yaml
image_upload_path: "docs/images"
```

<sup>1</sup>This currently still results in broken links in the Markdown. See caveats below.

## Configuration options

* `edit_branch` - The target branch to edit in the GitLab Static Site Editor (default: `master`)

## Caveats

Currently, the GitLab Static Site Editor does not mount the `docs/` directory as the root
when manually uploading images, causing broken links to images. This should be fixed when
GitLab parses the (currently undocumented) `mounts:` entry in `.gitlab/static-site-editor.yml`.
