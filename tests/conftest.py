import os

import pytest
from click.testing import CliRunner

from mkdocs.__main__ import build_command


@pytest.fixture(scope="function")
def mkdocs_build():
    def _mkdocs_build(tmp_path, mkdocs_config):
        mkdocs_yml = tmp_path / "mkdocs.yml"
        mkdocs_yml.write_text(mkdocs_config)

        docs_dir = tmp_path / "docs"
        docs_dir.mkdir()

        index = docs_dir / "index.md"
        index.write_text("# Home")

        os.chdir(tmp_path)
        runner = CliRunner()
        return runner.invoke(build_command)

    return _mkdocs_build
