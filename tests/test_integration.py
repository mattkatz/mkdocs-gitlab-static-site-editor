from urllib.parse import quote_plus


def text_in_page(tmp_path, text):
    with open(tmp_path / "site" / "index.html") as f:
        return text in f.read()


def test_build_skip_if_no_repo_url(tmp_path, mkdocs_build):
    mkdocs_config = """---
site_name: mkdocs-static-site-editor
# site_url: https://example.gitlab.io/example.git
plugins:
    - search
    - gitlab-static-site-editor
nav:
  - Home: index.md
"""

    result = mkdocs_build(tmp_path, mkdocs_config)
    assert result.exit_code == 0
    assert not text_in_page(tmp_path, "Edit on GitLab")


def test_build_correct_edit_url(tmp_path, mkdocs_build):
    mkdocs_config = """---
site_name: mkdocs-static-site-editor
repo_url: https://gitlab.com/example/example.git
site_url: https://example.gitlab.io/example.git
plugins:
    - search
    - gitlab-static-site-editor
nav:
  - Home: index.md
"""
    edit_url = "https://gitlab.com/example/example.git/-/sse/master%2Fdocs%2Findex.md?return_url=https%3A%2F%2Fexample.gitlab.io%2Fexample.git%2F"

    result = mkdocs_build(tmp_path, mkdocs_config)
    assert result.exit_code == 0
    assert text_in_page(tmp_path, edit_url)
