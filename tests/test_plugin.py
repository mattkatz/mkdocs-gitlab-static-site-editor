import os

import pytest
from mkdocs import config

from mkdocs_gitlab_editor.plugin import (
    _get_relative_docs_dir,
    DEFAULT_BRANCH,
    StaticSiteEditorPlugin,
)

default_config = {"edit_branch": DEFAULT_BRANCH}


def load_config(**cfg):
    """
    todo: reuse
    https://github.com/mkdocs/mkdocs/blob/ff0b7260564e65b6547fd41753ec971e4237823b/mkdocs/tests/base.py#L22
    """
    base_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))

    cfg = cfg or {}
    if "site_name" not in cfg:
        cfg["site_name"] = "Example"
    if "site_url" not in cfg:
        cfg["site_url"] = "https://example.com"
    if "config_file_path" not in cfg:
        cfg["config_file_path"] = os.path.join(base_dir, "mkdocs.yml")
    if "docs_dir" not in cfg:
        cfg["docs_dir"] = os.path.join(base_dir, "docs")
    conf = config.Config(
        schema=config.DEFAULT_SCHEMA, config_file_path=cfg["config_file_path"]
    )
    conf.load_dict(cfg)

    return conf


@pytest.mark.parametrize(
    "config_file_path,docs_dir,expected",
    [
        ("/home/repo/mkdocs.yml", "/home/repo/docs", "docs"),
        ("/home/repo/mkdocs.yml", "/home/repo/nested/docs", "nested%2Fdocs"),
    ],
    ids=["default docs_dir", "nested docs_dir"],
)
def test_get_relative_docs_dir(config_file_path, docs_dir, expected):
    conf = {}
    conf["config_file_path"] = config_file_path
    conf["docs_dir"] = docs_dir

    docs_dir = _get_relative_docs_dir(conf)
    assert docs_dir == expected


@pytest.mark.parametrize(
    "config_dict,expected_editor_url",
    [
        ({}, None),
        (
            {"repo_url": "https://gitlab.com/repo"},
            f"https://gitlab.com/repo/-/sse/{DEFAULT_BRANCH}%2Fdocs%2F",
        ),
        (
            {"repo_url": "https://gitlab.com/repo", "edit_branch": "main"},
            "https://gitlab.com/repo/-/sse/main%2Fdocs%2F",
        ),
    ],
    ids=["no repo_url", "default branch", "configured branch"],
)
def test_on_config(config_dict, expected_editor_url):
    plugin = StaticSiteEditorPlugin()
    plugin.load_config(config_dict)

    result = plugin.on_config(load_config(**config_dict))
    assert result.get("editor_url") == expected_editor_url
